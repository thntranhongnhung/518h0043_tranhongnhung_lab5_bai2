import '../repositories/repository.dart';
import 'package:flutter/foundation.dart';

class Task {
  final int id;
  String tenSp;
  String description;
  String price;
  String img;
  bool complete;

  Task({
    required this.id,
    this.complete = false,
    this.description = '',
    this.tenSp = '',
    this.price = '',
    this.img = '',
  });

  // model: properties id
  // model.data contains keys: description, complete
  Task.fromModel(Model model)
      : id = model.id,
        tenSp = model.data['tenSp'],
        price = model.data['price'],
        description = model.data['description'],
        img = model.data['img'],
        complete = model.data['complete'];

  Model toModel() =>
      Model(id: id, data: {'description': description, 'complete': complete});
}
