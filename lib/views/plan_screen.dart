import '../models/data_layer.dart';
import 'package:flutter/material.dart';

import '../plan_provider.dart';

class PlanScreen extends StatefulWidget {
  final Plan plan;
  const PlanScreen({Key? key, required this.plan}) : super(key: key);
  @override
  State createState() => _PlanScreenState();
}

class _PlanScreenState extends State<PlanScreen> {
  late ScrollController scrollController;
  Plan get plan => widget.plan;
  @override
  void initState() {
    super.initState();
    scrollController = ScrollController()
      ..addListener(() {
        FocusScope.of(context).requestFocus(FocusNode());
      });
  }

  @override
  Widget build(BuildContext context) {
    // final plan = PlanProvider.of(context);
    return Scaffold(
        appBar: AppBar(title: Text('Các sản phẩm trong danh mục '+plan.name)),
        body: Column(children: <Widget>[
          Expanded(child: _buildList()),
          SafeArea(child: Text(plan.completenessMessage))
        ]),
        floatingActionButton: _buildAddTaskButton());
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  Widget _buildAddTaskButton() {
    //final plan = PlanProvider.of(context);
    return FloatingActionButton(
      child: Icon(Icons.add),
      onPressed: () {
        final controller = PlanProvider.of(context);
        controller.createNewTask(plan);
        setState(() {});
      },
    );
  }

  Widget _buildList() {
    //final plan = PlanProvider.of(context);
    return ListView.builder(
      controller: scrollController,
      itemCount: plan.tasks.length,
      itemBuilder: (context, index) => _Sp(plan.tasks[index]),
    );
  }

  Widget _buildTaskTile(Task task) {
    return Dismissible(
      key: ValueKey(task),
      background: Container(color: Colors.red),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        final controller = PlanProvider.of(context);
        controller.deleteTask(plan, task);
        setState(() {});
      },
      child: ListTile(
        // leading: Checkbox(
        //     value: task.complete,
        //     onChanged: (selected) {
        //       setState(() {
        //         task.complete = selected as bool;
        //       });
        //     }),
        title: TextFormField(
          initialValue: task.tenSp,
          onFieldSubmitted: (text) {
            setState(() {
              task.tenSp = text;
            });
          },
        ),
      ),
    );
  }


  Widget _Sp(Task task){
    return Dismissible(
      key: ValueKey(task),
      background: Container(color: Colors.red),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        final controller = PlanProvider.of(context);
        controller.deleteTask(plan, task);
        setState(() {});
      },
      child: Expanded(
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          color: Colors.lightBlueAccent.withOpacity(0.5),
          child: Column(
            children: [
              Image(image: NetworkImage(task.img)),
              Center(
                child: TextFormField(
                  decoration: InputDecoration(
                    // icon: Icon(Icons.send),
                    hintText: 'Nhập tên sản phẩm',
                    // helperText: 'Helper Text',
                    // counterText: '0 characters',
                    border: InputBorder.none,
                    contentPadding: EdgeInsets.only(left: 15, right: 15),
                  ),
                  initialValue: task.tenSp,
                  onFieldSubmitted: (text) {
                    setState(() {
                      task.tenSp = text;
                    });
                  },
                ),
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Mô tả sản phẩm',
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 15, right: 15),
                ),
                initialValue: task.description,
                onFieldSubmitted: (text) {
                  setState(() {
                    task.description = text;
                  });
                },
              ),
              TextFormField(
                decoration: InputDecoration(
                  hintText: 'Giá sản phẩm',
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left: 15, right: 15),
                ),
                style: TextStyle(color: Colors.red),
                initialValue: task.price,
                onFieldSubmitted: (text) {
                  setState(() {
                    task.price = text;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

}
